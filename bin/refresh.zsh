#!/usr/local/bin/zsh

# 1. set yarn to the latest stable version
# 2. install yarn packages

set -e

#////////////////////////////////////////////
function do_refresh
{
  target_dir="$1"
 
  echo "----------------------------" 
  echo "doing refresh in $target_dir"
  echo "----------------------------" 
  cd "$target_dir"
  yarn set version stable && yarn install
  cd -
}
#////////////////////////////////////////////

do_refresh "../dist"
do_refresh "../test"

