"use strict";

/**
 * @dallmo/youtube/
 */

import { dallmo_yaml } from "dallmo-yaml";

//import { video, get_info, get_info_live } from "@dallmo/youtube/video";
import { video } from "@dallmo/youtube/video";
import { playlist } from "@dallmo/youtube/playlist";
import { channel } from "@dallmo/youtube/channel";

let config_obj;

/////////////////////////////////////////////////////
async function init( config_file ){

  config_obj = await dallmo_yaml( config_file );
  return config_obj;

}; // function init
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/");

}; // function tester
/////////////////////////////////////////////////////
const dallmo_youtube = {

  tester,
  
  init,
  config_obj,

  channel,
  playlist,
  video,

}; // youtube
/////////////////////////////////////////////////////
export {
  
  dallmo_youtube,
  tester,

  init,
  config_obj,
  
  channel,
  playlist,
  video,

}; // export
