"use strict";

/**
 * @dallmo/youtube/playlist 
 */

/////////////////////////////////////////////////////
function check_continuation( batch ){

  //console.log( "estimated total items : ", batch.estimatedItemCount );
  console.log( "check_continuation : item count of this batch : ", batch.items.length );

  if( batch.continuation == null ){
    console.log("check_continuation : >>> this is the last batch <<< " );
    return false;
  }else{
    console.log("check_continuation : continuation exists ... ");
    return true;
  }//if null

}//function check_continuation
/////////////////////////////////////////////////////////////////////
async function process_batch( this_batch, batch_count, item_array_prev ){

  // dynamic import cjs module
  const ytpl = import("ytpl");

  const log_divider = "-------------------------------------------------------";

  console.log( log_divider );
  batch_count += 1;
  console.log( "process_batch : batch count : ", batch_count );
  console.log( "process_batch : item array length of prev, start : ", item_array_prev.length );
  
  // the new array of this batch
  let item_array_new = item_array_prev.concat( this_batch.items );

  console.log( "process_batch : item array length after concat : ", item_array_new.length );

  // if continuation exists for this batch, recurse
  if( check_continuation( this_batch ) ){
      
    const next_batch = await ytpl.default.continueReq( this_batch.continuation );
    return await process_batch( next_batch, batch_count, item_array_new );

  }//recurse this function
  else{
    console.log( log_divider );
    console.log( "process_batch : item array length, return : ", item_array_new.length );
    console.log( log_divider );

    return item_array_new;
  };//if continuation exists

}//function process_batch
/////////////////////////////////////////////////////
async function get_all_video_items( option_obj ){

  /**
   * --------
   * OVERVIEW
   * --------
   * this function get every video item from a given playlist ID
   * 
   * all functions in this series only accept an option_obj for consistency 
   * 
   * ---------------------------
   * expected keys in option_obj
   * ---------------------------
   * option_obj = {
   *  playlist_id:
   * }
   * 
   */

  // dynamic import cjs module
  const ytpl = await import("ytpl");

  const playlist_id = option_obj.playlist_id;
  const batch_1 = await ytpl.default( playlist_id, { pages: 1 });
  
  let item_array = [], batch_count = 0;
      item_array = await process_batch( batch_1, batch_count, item_array );

  return item_array;

}; // get_video_item
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/playlist");

}; // function tester
/////////////////////////////////////////////////////
const playlist = {

  tester,
  get_all_video_items,
  
}; // math
/////////////////////////////////////////////////////
export {
  
  playlist,
  
  tester,
  get_all_video_items,

}; // export   
