"use strict";

/**
 * @dallmo/youtube/channel/func1
 *
 * get_all_playlists
 * 
 */

/////////////////////////////////////////////////////
function get_playlist() {

  console.log( "test message from : @dallmo/youtube/channel/funct1 - get_playlist() ... ~" );

}; // function get_playlist
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/channel");

}; // function tester
/////////////////////////////////////////////////////
const channel = {

  tester,
  get_playlist,

}; // math
/////////////////////////////////////////////////////
export {
  
  tester,
  get_playlist,

}; // export   
