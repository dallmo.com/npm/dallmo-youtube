"use strict";

/**
 * @dallmo/youtube/channel
 *
 * get_all_playlists
 * 
 */

import { get_playlist } from "./func1.mjs";


/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/channel");

}; // function tester
/////////////////////////////////////////////////////
const channel = {

  tester,
  get_playlist,

}; // math
/////////////////////////////////////////////////////
export {
  
  tester,
  channel,
  get_playlist,

}; // export   
