# dallmo-youtube

# overview
- util wrappers based on `ytdl-core`, `ytpl`, `ytsr`
- esm-native, to use this in cjs the module has to be imported dynamically ( usage code below );

## note
- as of `2023-09-12`, stick with version `4.9.1` for `ytdl-core` to avoid bugs in the fuction `getInfo`;


[link_1]: https://www.npmjs.com/package/rss-parser

# usage


## sample config file
```
---
```

## esm
```
"use strict";

import { youtube } from "@dallmo/youtube/sub"

```

## cjs
```
"use strict";

( async()=>{

  const youtube = await import("@dallmo/youtube/sub");

})(); // self-run async main
```