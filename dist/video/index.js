"use strict";

/**
 * @dallmo/youtube/video
 */

import * as search from "./search.js";
import * as info from "./info.js";
import * as download from "./download.js";

/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/video");

}; // function tester
/////////////////////////////////////////////////////
const search_all = search.search_all;
const get_info = info.get;
const get_info_live = info.get_live;
const download_one = download.download_one;
/////////////////////////////////////////////////////
const video = {

  tester,
  search_all,
  get_info,
  get_info_live,
  download_one,
  
}; // math

/////////////////////////////////////////////////////
export {
  
  video,
  
  tester,
  search_all,
  get_info,
  get_info_live,
  download_one,
  
}; // export   
