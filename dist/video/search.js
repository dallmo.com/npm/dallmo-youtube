"use strict";

/**
 * @dallmo/youtube/video - search
 */

import { filter } from "./filter.js";

/////////////////////////////////////////////////////
async function search_all( option_obj ){

  /**
   * --------
   * OVERVIEW
   * --------
   * this searches for videos with keyword and channel ID
   * as some videos may not have been organized / grouped inside the same playlist
   * 
   * all functions in this series only accept an option_obj for consistency
   * 
   * ---------------------------
   * expected keys in option_obj
   * ---------------------------
   * option_obj = {
   *  keyword:
   *  channel_id:
   * }
   * 
   */

  let array_filtered = [];

  // search string is required
  if( option_obj.keyword == undefined ){
    console.log("option_obj.keyword is missing. action aborted.");
    return array_filtered;
  }; //

  // load ytsr via dynamic import
  const ytsr = await import("ytsr");

  const keyword = option_obj.keyword;
  const channel_id = option_obj.channel_id;

  // the array to carry search result and be returned
  let array_match_channel_id = [];
  const options = {
    pages: Infinity,
  }; // options

  // the raw search results
  const searchResults = await ytsr.default( keyword, options );

  // array of items in searchResults.items
  // manully filter the result by keyword and channel_id
  for( const each_item of searchResults.items ){
    try{    
      if( each_item.author.channelID === channel_id ){
          array_match_channel_id.push( each_item );
        }; // if
    }catch(error){
      // just ignore it
    };// try catch
  }; // for

  // further filter the array 
  // by matching keyword in title
  option_obj.item_array = array_match_channel_id;
  array_filtered = filter.by_title( option_obj );

    return array_filtered;
  
}; // function search_all
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/video - search");

}; // function tester
/////////////////////////////////////////////////////
const search = {

  tester,
  search_all,

}; // math
/////////////////////////////////////////////////////
export {
  
  search,

  tester,
  search_all,

}; // export   
