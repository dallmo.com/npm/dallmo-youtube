"use strict";

/**
 * @dallmo/youtube/download
 */

// ref : https://nodejs.dev/en/api/v18/fs/
// fs sync APIs
import * as fs from 'node:fs';
import { pipeline } from 'node:stream/promises';

import ytdl from "ytdl-core";

/////////////////////////////////////////////////////
async function write_log( option_obj ){

  /**
   * write the log in the format of :
   * 1. divider : ---------------------------
   * 2. video info :
   *  - title
   *  - id
   *  - url
   * 3. start : timestamp, start
   * 4. end : timestamp, end
   */
  let write_log_result;
  try{
    
    console.log("write log : started, filename : ", option_obj.log_filename );
    const log_filename = option_obj.log_filename;

    write_log_result = "write log : succeed.";

  }catch(error){

    console.log("write log : FAILED : option_obj : ", option_obj );
    console.log("write log : FAILED : error message : ", error.message );
    write_log_result = error.message;

  }finally{
    return write_log_result;
  };


}; // function write_log
/////////////////////////////////////////////////////
function gen_filename( option_obj ){

  option_obj = gen_filename_log(   option_obj );
  option_obj = gen_filename_video( option_obj );

  return option_obj;

}; // function gen_filename
/////////////////////////////////////////////////////
function gen_filename_log( option_obj ){

  /**
   * add the property "log_filename" in option_obj and return
   * assume log is plaintext, with the extension "log"
   */

  const file_extension = ".log";
  const filename_base  = gen_filename_base( option_obj );
  const filename       = filename_base.concat( file_extension );

        // for log
        option_obj.log_filename = filename;
  
  return option_obj;

}; // function gen_filename_log
/////////////////////////////////////////////////////
function gen_filename_video( option_obj ){
  
  /**
   * add the property "video_filename" in option_obj and return
   * assume video format in "mp4"
   */

  const file_extension = ".mp4";
  const filename_base  = gen_filename_base( option_obj );
  const filename       = filename_base.concat( file_extension );

        // for video
        option_obj.video_filename = filename;
  
  return option_obj;

}; // function gen_filename_video
/////////////////////////////////////////////////////
function gen_filename_base( option_obj ){

  /**
   * --------
   * OVERVIEW
   * --------
   * generate a filename based on the info expected from option_obj
   * 
   * ---------------------------
   * expected keys in option_obj
   * ---------------------------
   * option_obj = {
   *  date:
   *  id: 
   * }
   * 
   */
  
  let filename;
  /**
   * compose the filename out of "date" and "id"
   * omit the extension for now
   */
  const video_date = option_obj.date;
  const video_id = option_obj.id;

    filename = video_date.concat( "--", video_id );

  return filename;

}; // function gen_filename
/////////////////////////////////////////////////////
async function download_one( option_obj ){

  /**
   * --------
   * OVERVIEW
   * --------
   * this function download a single video from a given video url
   * no format choosing ( available via ytdl-core though )
   * just assume the default format to keep things simple
   * 
   * all functions in this series only accept an option_obj for consistency 
   * 
   * ---------------------------
   * expected keys in option_obj
   * ---------------------------
   * option_obj = {
   *  date:
   *  id:
   *  url:
   * }
   * 
   */

  // generate filename for download and log
  option_obj = gen_filename( option_obj );

  const video_url      = option_obj.url;
  const video_filename = option_obj.video_filename;
  
  // start the video download
  let download_result;
  try{
    
    // 1. download video
    console.log("video download : started with the filename : ", option_obj.video_filename );
    
    /**
     * ytdl create a readStream
     * save that as video file with fs writeStream
     * and handle the whole process with pipeline
     */
    await pipeline(
      ytdl( video_url ),
      fs.createWriteStream( video_filename )
    ); // pipeline

    console.log( "video download : succeed" );
    
    // 2. write log, if video download succeed
    const write_log_result = await write_log( option_obj );
    console.log( write_log_result );
    
    download_result = "video download : completed with log file updated.";

  }catch(error){

    console.log("video download : FAILED : option_obj : ", option_obj );
    console.log("video download : FAILED : ", error.message);
    download_result = error.message;

  }finally{

    return download_result;

  };// try catch

}; // function download_one
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/download");

}; // function tester
/////////////////////////////////////////////////////
const download = {

  tester,
  download_one,

}; // math
/////////////////////////////////////////////////////
export {
  
  download,

  tester,
  download_one,

}; // export   
