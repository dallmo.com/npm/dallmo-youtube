"use strict";

/**
 * @dallmo/youtube/video - filter
 */

/////////////////////////////////////////////////////
function by_title( option_obj ){

  /**
   * --------
   * OVERVIEW
   * --------
   * this filter items array resulted from searching via "seach-video.js"
   * 
   * all functions in this series only accept an option_obj for consistency 
   * 
   * ---------------------------
   * expected keys in option_obj
   * ---------------------------
   * option_obj = {
   *  item_array:
   *  keyword:
   * }
   * 
   */

  const item_array = option_obj.item_array;
  const keyword = option_obj.keyword;

  let array_title_with_keyword = [];

  for( const each_item of item_array ){
    if( each_item.title.includes( keyword )){
      array_title_with_keyword.push( each_item );
    }; // if
  }; // for

  return array_title_with_keyword;

}; // function by_title
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/filter");

}; // function tester
/////////////////////////////////////////////////////
const filter = {

  tester,
  by_title,
  
}; // math
/////////////////////////////////////////////////////
export {
  
  tester,
  filter,
  by_title,

}; // export   
