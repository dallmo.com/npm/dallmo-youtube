"use strict";

/**
 * @dallmo/youtube/video
 */

/////////////////////////////////////////////////////
async function get ( option_obj ){

  /**
   * --------
   * OVERVIEW
   * --------
   * this function get basic video info
   * and return them as an info object
   * 
   * all functions in this series only accept an option_obj for consistency 
   * 
   * ---------------------------
   * expected keys in option_obj
   * ---------------------------
   * option_obj = {
   *  url: 
   * }
   * 
   */

  // load cjs module dynamically
  const ytdl = await import("ytdl-core");
  
  // define the vars to proceed
  const video_url = option_obj.url;
  const video_info = await ytdl.default.getInfo( video_url );
  
  // the list of info to capture in the return object
  //console.log( "video info, details : ", video_info.videoDetails );
  
  const video_date   = video_info.videoDetails.publishDate;
  const video_title  = video_info.videoDetails.title;
  const video_length = video_info.videoDetails.lengthSeconds;
  const video_id     = video_info.videoDetails.videoId;
  const channel_id   = video_info.videoDetails.channelId;
  const view_count   = video_info.videoDetails.viewCount;

  const result_obj = {
    date: video_date,
    title: video_title,
    length: video_length,
    id: video_id,
    channel_id: channel_id,
    view_count: view_count,
    url: video_url,
  }; // result

    return result_obj
  
}; // function get
/////////////////////////////////////////////////////
async function get_live ( option_obj ){

  /**
   * --------
   * OVERVIEW
   * --------
   * this function get basic video info
   * and return them as an info object
   * 
   * all functions in this series only accept an option_obj for consistency 
   * 
   * ---------------------------
   * expected keys in option_obj
   * ---------------------------
   * option_obj = {
   *  url:
   * }
   * 
   */

  // load cjs module dynamically
  const ytdl = await import("ytdl-core");

  const video_url = option_obj.url;
  const video_info = await ytdl.default.getInfo( video_url );
  
  //console.log( video_info, { depth: null } );

  // like count
  const like_count_string = video_info.response.contents.twoColumnWatchNextResults.results.results.contents[0].videoPrimaryInfoRenderer.videoActions.menuRenderer.topLevelButtons[0].segmentedLikeDislikeButtonRenderer.likeCount;
  
    const like_count_num = Number( like_count_string );
    //console.dir( like_count_num, { depth: null } );

  // watching now view count
  const view_count_string = video_info.response.contents.twoColumnWatchNextResults.results.results.contents[0].videoPrimaryInfoRenderer.viewCount.videoViewCountRenderer.originalViewCount;

    const view_count_num = Number( view_count_string )
    //console.dir( view_count_num, { depth: null } );

  const video_details  = video_info.videoDetails;
    const video_date   = video_details.publishDate;
    const video_title  = video_details.title;
    
    let video_id;
    ( video_details.videoId === undefined )?
    ( video_id = "video_id_null" ) : 
    ( video_id = video_details.videoId );

    
  /**
   * in case the video itself has never been live, i.e. only recorded
   * i.e. these parameters won't be false but undefined
  */
  let is_live, live_started;
  if( video_info.videoDetails.liveBroadcastDetails === undefined ){

    is_live = false;
    live_started = 0;

  }else{
    
    const video_details_live = video_info.videoDetails.liveBroadcastDetails;
      is_live = video_details_live.isLiveNow;
      live_started = video_details_live.startTimestamp;

  }; // check if liveBroadcastDetails exists
  
  //..................................
  const result_obj = {
    date: video_date,
    title: video_title,
    id: video_id,
    view_count: view_count_num,
    like_count: like_count_num,
    is_live: is_live,
    live_started: live_started,
  }; // result
  //..................................

    return result_obj
  
}; // function get
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/youtube/video - info");

}; // function tester
/////////////////////////////////////////////////////
const video = {

  tester,
  get,
  get_live,

}; // math
/////////////////////////////////////////////////////
export {
  
  video,

  tester,
  get,
  get_live,

}; // export   
