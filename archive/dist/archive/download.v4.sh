#!/bin/bash

while read each_item
do

  video_index="$( echo $each_item | awk '{print $1}' )"
  video_id="$( echo $each_item | awk '{print $2}' )"
  video_url="$( echo $each_item | awk '{print $3}' )"

  # get publish date
  video_date="$( ytdl --info-json "$video_url" | jq '.videoDetails.publishDate' | sed -e s/\"//g )"
 
  timestamp="$( date +"%F_%H%M%S" )"
  filename_video="$video_date"'_'"$video_id"'.mp4'
  filename_info="$filename_video"'.info'

    # get video info
    ytdl "$video_url" --info > "$filename_info"
  
    # get video
    ytdl "$video_url" --output "$filename_video"

    msg_index="item index : $video_index"
    msg_id="item id : $video_id"
    msg_url="item url : $video_url"
    msg_date="publish date : $video_date"
    msg_timestamp="timestamp : $timestamp"

  echo "------------------------------------" | tee -a "$filename_info"
  echo "output file : $filename_video"
  echo "$msg_index" | tee -a "$filename_info"
  echo "$msg_id"    | tee -a "$filename_info"
  echo "$msg_url"   | tee -a "$filename_info"
  echo "$msg_date"  | tee -a "$filename_info"
  echo "$msg_timestamp" | tee -a "$filename_info"

done< <( cat "video.list" )

