'use strict';

// external modules
const read_config = require('dallmo-read-config');

// classes of this module
const Playlist = require('./youtube/Playlist.js');
const Video    = require('./youtube/Video.js');

/*
  ref : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/static
*/

//##################################################
class Youtube{
 
  //==========================================================
  constructor(){

  }//constructor
  //==========================================================
  async init( config_file ){

    const config_obj = await read_config( config_file );
      this.playlist = new Playlist( config_obj );
      this.video    = new Video();

  }//function
  //==========================================================

}//class
//##################################################

module.exports = Youtube;

