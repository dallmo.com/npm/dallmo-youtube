
const fs = require('fs');
const ytpl = require('ytpl');

//########################################################################################
async function process_batch( para ){

  const this_batch = para.batch;
  let batch_count = para.count;
  const item_array_prev = para.array;

  //console.log( "----------------");
  batch_count += 1;
  console.log( "process : batch count : ", batch_count );
  //console.log( "process : item array length of prev, start : ", item_array_prev.length );

  // the new array of this batch
  let item_array_new = item_array_prev.concat( this_batch.items );

  //console.log( "process : item array length after concat : ", item_array_new.length );

  // if continuation exists for this batch, recurse
  if( check_continuation( this_batch ) ){

    const next_batch = await ytpl.continueReq( this_batch.continuation );
    const next_para = {
      batch: next_batch,
      count: batch_count,
      array: item_array_new,
    };
    return await process_batch( next_para );

  }//recurse this function
  else{
    console.log( "process : item array length, return : ", item_array_new.length );
    return item_array_new;
  };//if continuation exists

}//function

//########################################################################################
function check_continuation( batch ){

  //console.log( "estimated total items : ", batch.estimatedItemCount );
  //console.log( "check : item count of this batch : ", batch.items.length );

  if( batch.continuation == null ){
    console.log("check : >>> this is the last batch <<< " );
    return false;
  }else{
    //console.log("check : continuation exists ... ");
    return true;
  }//if null

}//function

//########################################################################################
function print_batch( batch ){
  batch.items.forEach( each_item => {
    print_item( each_item );
  });
}//function

function print_item_array( item_array ){
  item_array.forEach( each_item => {
    print_item( each_item );
  });
}//function

function print_item( each_item ){
  //console.log( each_item.index, "\t", each_item.id, "\t", each_item.url );
  console.log( each_item.index, " : ", each_item.title );
}//function
//########################################################################################
function mkdir( _dir ){
  
  let result_obj = {};  

  if (fs.existsSync( _dir )){
    // do nothing
  }else{
    fs.mkdirSync( _dir );
  }//mkdir if the directory isn't there yet

}//function
//########################################################################################

module.exports = {
  mkdir,
  process_batch,
  check_continuation,
  print_batch,
  print_item,
  print_item_array,
};

