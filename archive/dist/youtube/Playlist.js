'use strict';

// external modules
const ytpl = require('ytpl');

// local modules for this class
const util = require('./util.js');
const Video = require('./Video.js');

//##############################################
class Playlist{

  //==================================================================
  constructor( config_obj ){

    this.list_id = config_obj.list_id;

    this.title = {};
    this.title.chi = config_obj.title.chi;
    this.title.eng = config_obj.title.eng;

    // loaded from local module, util functions
    this.util = util;

    this.download_range = {};

    this.log_div = "..............................................";

  }//constructor
  //==================================================================
  #download_range_reset(){

      this.download_range.start = 1;
      this.download_range.end   = this.total_video_count;

  }//function
  //==================================================================
  #validate_download_range(){

    // this function make sure the download range is
    // valid by considering the result length    

    // all of the below conditions must be met :
    // 1. both range num are integers
    // 2. start range larger than or equal to 1
    // 3. end range smaller than or equal to length of result_array
    // 4. start range smaller than or equal to end range

  }//function
  //==================================================================
  async #set_download_range( range_obj = {} ){
  
    config.log("range obj : ", range_obj);

    // this function defines the range of video to download by index
    await this.#get_total_video_count();
 
    //.............................................
    try{
      console.log( "range_obj : ", range_obj );
    }catch(error){
      console.log( "error in showing range_obj" );
    }// try catch
   
    //.............................................
    // define the download range
    if( range_obj.hasOwnProperty( 'start' )){
      this.download_range.start = range_obj.start;
    }else{
      this.download_range.start = 1;
    }//range start

    //.............................................
    if( range_obj.hasOwnProperty( 'end' )){
      this.download_range.end = range_obj.end;
    }else{
      this.download_range.end = this.total_video_count;
    }//range end

    //.............................................
    this.#validate_download_range();
    console.log( "download range set : ", this.download_range );

  }//
  //==================================================================
  async #get_total_video_count(){

    if( this.result_array ){
      // downloaded already, skip this
      return;
    }else{
      console.log( "getting total video count ... " );
      const batch_1 = await ytpl( this.list_id, { pages: 1 });

      let item_array = [], batch_count = 0;
      const para_obj = {
        batch: batch_1,
        count: batch_count,
        array: item_array,
      };

      this.result_array = await this.util.process_batch( para_obj );
      this.total_video_count = this.result_array.length ; 
      console.log( "total video count is : ", this.total_video_count );
    }// if else

  }//function
  //==================================================================
  #download_range_exists(){

    // this function check if the download range had been defined
    const has_range_start = ( this.download_range.hasOwnProperty('start'));
    const has_range_end   = ( this.download_range.hasOwnProperty('end'));

      return ( has_range_start && has_range_end );

  }//
  //==================================================================
  // the only exposed public method
  async download( range_obj = {} ){

    await this.#set_download_range( range_obj );
  
    console.log( "downloading all video in the list : ", this.list_id );
    console.log( "with the download range : ", this.download_range );
  
    try{
      // loop thro' the result array
      for( const each_item of this.result_array ){
        await this.#download_each_item( each_item );
      }//for

      console.log( this.log_div );
      console.log( "download completed." );
    }catch(error){
      console.log( " error in download : ", error );
      // to-do : log the error
    }//try catch : download

  }//function
  //==================================================================
  #video_index_out_of_range( _index ){

    // check if the current video index is within 
    // the defined download range
    // start_ok : large than or equal to range start
    // end_ok : small than or equal to range end
    const start_ok = ( _index >= this.download_range.start );
    const end_ok = ( _index <= this.download_range.end );

      return !( start_ok && end_ok );

  }//function
  //==================================================================
  async #download_each_item( video_item ){

    const index = video_item.index.toString();
    const count = this.total_video_count.toString();
    console.log( this.log_div );
    console.log( "downloading ", index + "/" + count );

    //check download range
    if( this.#video_index_out_of_range( index )){
      console.log( "accepted video index range : ", this.download_range.start, " - ", this.download_range.end );
      console.log( "current video index is out of range, skipped." );
      return;
    }// exits if out of range

    // move on if the index is within download range
    const para_obj = {
      video_item: video_item,
      total_video_count: this.total_video_count,
      playlist_title: this.title,
    };

    // Video : a subclass written for this module
    //         for video-related operations
    let curr_video_item = new Video();
        curr_video_item.init( para_obj );

        // this includes both : 
        //    "download_info" : get the descriptions and text info only
        //    "download_video" : get the video file
        await curr_video_item.download();
              curr_video_item = null; // clean things up

  }//function
  //==================================================================

}//class
//##############################################

module.exports = Playlist;

