'use strict';

// built-in modules
const path = require('path');

// external modules
const ytdl = require('ytdl-core');
const dallmo_fs = require('dallmo-fs');

const cli_progress = require('cli-progress');


//##############################################
class Video{

  //==============================================================
  constructor(){

    // empty constructor
    // for this class to be called by both the youtube package and Playlist class

  }//constructor
  //==============================================================
  init( para_obj ){

    this.total_video_count = para_obj.total_video_count;
    this.playlist_title = para_obj.playlist_title;

      this.download_directory = "video--" + this.playlist_title.eng;
      this.util.mkdir( this.download_directory );

    const video_item = para_obj.video_item;
      // the format of "video_item" is defined by the output result
      // from the module "ytpl", loaded in "util.js"

      this.title = video_item.title;
      this.url   = video_item.url;

      // the random string which uniquely represent the video
      this.id    = video_item.id;

      // the integer which increment along the video count
      // quite often not equal to total downloadable video count 
      // 'cause some video could be private / not available
      this.index = video_item.index;

  }//function
  //==============================================================
  async download_info(){

      // saving the video text-based info in yaml format
      this.save_info_yaml();

  }//function
  //==============================================================
  async download_video(){

    // prepare the cli progress bar
    const progressBar = new cli_progress.SingleBar({
        format: '{bar} {percentage}% | ETA: {eta}s'
    }, cli_progress.Presets.shades_classic);

    let bytes_received = 0

    // prepare filenames
    const filename = this.filename_base + ".mp4";
    const full_path = path.join( this.download_directory, filename );
    
    console.log( "download_video data / finish / close : ..." );
    console.log( "video url : ", this.url );
    console.log( "video filename : ", filename );
    console.log( "video fullpath : ", full_path );

    // do the download of video file
    ytdl( this.url )
      .on('response', (response) => {
          if (response.statusCode !== 200) {
              return callback('Response status was ' + response.statusCode);
          }

          const bytes_total = response.headers['content-length'];
          console.log( "total bytes : ", bytes_total );
          
          progressBar.start(bytes_total, 0);
      })
      .on('data', (chunk) => {
          bytes_received += chunk.length;
          progressBar.update(bytes_received);
      })
      .pipe(fs.createWriteStream( full_path ))
      .on('finish', ()=> console.log( 'video download : stream finish' ) )
      // generate sha256sum when download complete
      .on('close',  ()=> { 
        console.log( 'video download : stream close'  );
        console.log( 'video download completed, generate sha256sum now ...' );
        console.log( 'sha256sum generated.  all done.' );
      });// ytdl

  }//function
  //==============================================================
  async download(){

    //..................................................
    try{

      const info = await ytdl.getInfo( this.url );
      this.update_info( info );

      // the format of "video_item" is defined by the output result
      // from the module "ytpl", loaded in "util.js"
      console.log( "download_info : " );
      console.log( "index : ", this.index );
      console.log( "title : ", this.title );
      console.log( "id : ", this.id );
      console.log( "publish date : ", this.publish_date );

      //this.print_info();
<<<<<<< HEAD
      this.#save_info_yaml();
=======
      this.download_info();
      this.download_video();
>>>>>>> b9b6566ce2e71ae50fc865e288066a566e9fde78

    //..................................................
    }catch(error){

      console.log( "error in downloading : ", error );
      // to-do : log the error in text file

    }//try catch error

  }//function
  //==============================================================
  update_info( video_info ){

    const info = video_info.videoDetails ; 
  
      this.publish_date = info.publishDate;
      this.desc = info.description ; 
      this.length_in_seconds = info.lengthSeconds ; 
      this.view_count = info.viewCount ; 
      this.keywords = info.keywords;

      // basename is shared for both video text file and video mp4 file
      this.filename_base = this.publish_date + "_" + this.id;

      // format info as multi-line text for saving / console log check
      this.#format_info_yaml();

  }//function
  //==============================================================
  print_info(){

    console.log( "--------------------------------" );
    console.log( " info for saving to text file : " );
    console.log( this.info_for_saving.yaml );

  }//function
  //==============================================================
  #format_info_yaml(){

    // format the video info to save in yaml
    this.info_for_saving = {};
    this.info_for_saving.yaml = '---' + "\n";
    
    this.#add_line( "index: " + this.index );
    this.#add_line( "total_video_count: " + this.total_video_count );
    this.#add_line( "id: " + this.id );
    this.#add_line( "title: " + this.title );
    this.#add_line( "length_in_seconds: " + this.length_in_seconds );
    this.#add_line( "publish_date: " + this.publish_date );
    this.#add_line( "keywords: " + this.keywords );
    this.#add_line( "url: " + this.url );
    this.#add_line( "desc: " + this.desc );

  }//function
  //==============================================================
  #add_line( new_line ){
  
    this.info_for_saving.yaml += new_line;
    this.info_for_saving.yaml += "\n";
  
  }//function
  //==============================================================
  async #save_info_yaml(){

    // prepare for the saving, including directory and base filename
    // base filename is shared for both video info text and video mp4 file


    // save the video info as text file, in yaml format

    // prepare directory
<<<<<<< HEAD
    const directory = "video-info-" + this.playlist_title.eng;

    // prepare filename
    const filename = this.publish_date + "_" + this.id + ".yaml";
=======
    //const directory = "video--" + this.playlist_title.eng;
    //const directory = "/123123" ; // testing for file saving error catch
    //this.util.mkdir( directory );

    // prepare filename
    //const filename = this.publish_date + "_" + this.id + ".yaml";
    const filename = this.filename_base + ".yaml";

    //console.log( "saving info to file : ", filename );   
    
    // prepare fullpath
    const full_path = path.join( this.download_directory, filename );
>>>>>>> b9b6566ce2e71ae50fc865e288066a566e9fde78

    // prepare the path and content as object
    const save_para = {
      path: directory,
      filename: filename,
      content: this.info_for_saving.yaml,
    };

    // save the text file
    const result_obj = await dallmo_fs.save( save_para );

      console.log( "file saving result for : ", filename, " : ", result_obj.status );

      return result_obj;

  }//function
  //==============================================================

}//class
//##############################################

module.exports = Video;

