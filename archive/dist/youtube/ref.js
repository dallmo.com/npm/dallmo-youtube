'use strict';

// modules
const dallmo_read_config = require('dallmo-read-config');

// subclasses
const Board = require('./Trello/Board.js');
const Card  = require('./Trello/Card.js');
const Util  = require('./Trello/Util.js');

/*
  ref : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/static
*/

//##################################################
class Trello{

  //==============================================
  constructor(){

    this.util = Util;

  }//constructor
  //==============================================
  async init( config_file, mode="dev" ){
    
    dallmo_read_config( config_file )
      .then( config_obj =>{
          this.mode = mode;
          this.#set_config( config_obj );
      });// read config promise

  }//init
  //==============================================
  // # private function
  #set_config( config_obj ){

    const api = config_obj.api;

    // determine certain vars by mode
    let card_id_curr;
    if( this.mode == "prod" ){
      card_id_curr = config_obj.card_id.prod;
    }else{
      card_id_curr = config_obj.card_id.dev;
    }//if else, mode

    let reformat_config = {
      api_url: api.url,
      auth_suffix: "key=".concat( api.key, "&token=", api.token ),
      board_id: config_obj.board_id,
      card_id:  card_id_curr,
      canvas:   config_obj.canvas,
    };

    this.config_obj = reformat_config;

    this.board = new Board( this.config_obj );
    this.card  = new Card( this.config_obj );

  }//set_config
  //==============================================

}//class
//##################################################

module.exports = Trello;

