"use strict";

/////////////////////////////////////////////////////////////////////
async function search_video( option_obj ){

  /**
   * --------
   * OVERVIEW
   * --------
   * this searches for videos with keyword and channel ID
   * as some videos may not have been organized / grouped inside the same playlist
   * 
   * all functions in this series only accept an option_obj for consistency
   * 
   * ---------------------------
   * expected keys in option_obj
   * ---------------------------
   * option_obj = {
   *  keyword:
   *  channel_id:
   * }
   * 
   */

  // load ytsr via dynamic import
  const ytsr = await import("ytsr");

  const keyword = option_obj.keyword;
  const channel_id = option_obj.channel_id;

  // the array to carry search result and be returned
  let array_match_channel_id = [];
  const options = {
    pages: Infinity,
  }; // options

  // the raw search results
  const searchResults = await ytsr.default( keyword, options );

  // manully filter the result by keyword and channel_id
  for( const each_item of searchResults.items ){
    try{    
      if( each_item.author.channelID === channel_id ){
          array_match_channel_id.push( each_item );
        }; // if
    }catch(error){
      // just ignore it
    };// try catch
  }; // for

  return array_match_channel_id;

}; // search_video
/////////////////////////////////////////////////////////////////////
export {

  search_video,
  
}; // export
