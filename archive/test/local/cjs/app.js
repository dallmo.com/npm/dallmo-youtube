
const dallmo_youtube = require("dallmo-youtube");
const config_file = "./config.yaml";

//=============================================
// optionally defines the download range
// the video download range, by index,
// which starts from 1 to video count
const range_obj = {
  start: 1,
  //end: 1,
};

const dt = new dallmo_youtube();
const promise_init      = dt.init( config_file );
const promise_set_range = () => dt.playlist.set_download_range( range_obj );

      promise_init
        .then( promise_set_range )
        .then( ()=> {
          dt.playlist.download();
        });//promise_init.then
