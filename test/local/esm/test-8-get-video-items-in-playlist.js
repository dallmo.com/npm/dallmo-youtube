"use strict";

import { dallmo_youtube } from "@dallmo/youtube";

dallmo_youtube.tester();

// read config file
const config_file = "./etc/config.yaml";
const config_obj = await dallmo_youtube.init( config_file );
const playlist_id = config_obj.playlist_id;


let option_obj = {};
    option_obj.playlist_id = playlist_id;
    const array_video_items = 
      await dallmo_youtube.playlist.get_all_video_items( option_obj );
    
    console.log( "array_video_items : ", array_video_items );
