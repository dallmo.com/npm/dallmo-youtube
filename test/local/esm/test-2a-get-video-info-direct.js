"use strict";

import { dallmo_youtube } from "@dallmo/youtube";

dallmo_youtube.tester();

let option_obj = {};
const video_url = "https://www.youtube.com/watch?v=JYEutL_zQQc";
option_obj.url = video_url;

const video_info = await dallmo_youtube.video.get_info( option_obj );
console.log( video_info );

option_obj.date = video_info.date;
option_obj.id = video_info.id;
option_obj.channel_id = video_info.channel_id;


