"use strict";

import { dallmo_youtube } from "@dallmo/youtube";

dallmo_youtube.tester();
dallmo_youtube.video.search.tester();

const config_file = "./etc/config.yaml";

const config_obj = await dallmo_youtube.init( config_file );
const option_obj = {
  keyword: config_obj.title_keyword,
  channel_id: config_obj.channel_id,
}; // option_obj

// make the search based on the config_file
const search_result = await dallmo_youtube.video.search_all( option_obj );
  console.log( search_result );


