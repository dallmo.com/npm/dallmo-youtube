"use strict";

import { dallmo_youtube } from "@dallmo/youtube";

dallmo_youtube.tester();
dallmo_youtube.search.tester();
dallmo_youtube.filter.tester();
dallmo_youtube.get_info.tester();
dallmo_youtube.download.tester();

const config_file = "./etc/config.yaml";

const config_obj = await dallmo_youtube.init( config_file );

let option_obj;
option_obj = {
  keyword: config_obj.title_keyword,
  channel_id: config_obj.channel_id,
}; // option_obj


// make the search based on the config_file
const array_search_result = await dallmo_youtube.search_video( option_obj );
//  console.log( array_search_result );

for( const each_item of array_search_result ){

  const video_url = each_item.url;
  option_obj.url = video_url;

  const video_info = await dallmo_youtube.get_video_info( option_obj );
  console.log( video_info );

}; // for of array_search_result

/*
const config_obj = dallmo_yaml( config_file );
  console.log( config_obj );
*/


