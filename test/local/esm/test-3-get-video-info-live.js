"use strict";

import { dallmo_youtube } from "@dallmo/youtube";

dallmo_youtube.tester();
dallmo_youtube.video.tester();

let option_obj, video_info, video_url;

///////////////////////////////////////////
async function check_live_info( video_url ){

  option_obj = {
    url: video_url
  };
  
  const video_info_live = await dallmo_youtube.video.get_info_live( option_obj );
  const video_info      = await dallmo_youtube.video.get_info( option_obj );

    console.log( "video_info_live : ", video_info_live );
    console.log( "video_info : ",      video_info      );
  
}; // check_live_info
///////////////////////////////////////////


// url is a live video
video_url = "https://www.youtube.com/watch?v=coYw-eVU0Ks"
console.log( "1. is live" );
await check_live_info( video_url );

// the video was live but not now
video_url = "https://www.youtube.com/watch?v=B8Iol3V5XLk";
console.log( "2. was live, not now" );
await check_live_info( video_url );

// url has never been a live video
video_url = "https://www.youtube.com/watch?v=u-gLeusDd-w";
console.log( "3. has never been live" );
await check_live_info( video_url );
