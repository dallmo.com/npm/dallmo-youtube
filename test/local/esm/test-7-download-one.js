"use strict";

import { dallmo_youtube } from "@dallmo/youtube";

dallmo_youtube.tester();


const config_file = "./etc/config.yaml";

const config_obj = await dallmo_youtube.init( config_file );
//console.log( config_obj );

let option_obj = {};
const video_url = config_obj.video_url;
  option_obj.url = video_url;

const video_info = await dallmo_youtube.video.get_info( option_obj );
//  console.log( video_info );

  const download_option_obj = {
    date:  video_info.date,
    title: video_info.title,
    id:    video_info.id,
    url:   video_info.url,
  };

// download video
const download_result = await dallmo_youtube.video.download_one( download_option_obj );
  console.log( download_result );
