"use strict";

( async()=>{

  // local the esm module dynamic import
  const dallmo_youtube = await import("@dallmo/youtube");

  dallmo_youtube.tester();
  dallmo_youtube.video.tester();
  
})(); // self-run async main
